from flask import Flask, Response
import json

app = Flask(__name__)

@app.route('/', methods=['GET'])
def home():
    data = json.dumps({"message":"hello world v1.0"})
    return Response(data , mimetype="application/json")


if __name__ == '__main__':
    #app.run(debug=True) # for dev
    app.run(host='0.0.0.0', port=5000)
